package com.gumballmachine;

/**
 * Created by jassenmoran on 9/17/15.
 */
public abstract class TurnstileFSM {
    private State state;
    private int count = 0;

    public void insertQuarter() { state.insertQuarter(this); }
    public void ejectQuarter() { state.ejectQuarter(this); }
    public void setState(State state) { this.state = state;}
    public void turnCrank() { state.turnCrank(this); state.dispense(this); }
    public void dispense() { state.ejectQuarter(this); }

    public int getCount()
    {
        return count;
    }
    public void setCount(int count) { this.count = count; }
    public void releaseBall()
    {
        System.out.println("A gumball comes rolling out the slot...");
        if (count != 0)
        {
            count--;
        }
    }
    
}

