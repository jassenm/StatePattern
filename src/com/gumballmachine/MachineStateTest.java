package com.gumballmachine;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by jassenmoran on 9/17/15.
 */

public class MachineStateTest extends TurnstileFSM {
    TurnstileFSM fsm;

    @Before
    public void setup() {
        fsm = this;
        fsm.setCount(5);
        fsm.setState(MachineState.NO_QUARTER);
    }

    @Test
    public void normalOperation() throws Exception {
        assertEquals(5, fsm.getCount());
        insertQuarter();
        turnCrank();
        int newcount = fsm.getCount();
        assertTrue(newcount < 5);
    }

    @Test
    public void ejectPayment() throws Exception {
        insertQuarter();
        ejectQuarter();
        turnCrank();
        assertEquals(5, fsm.getCount());
    }

    @Test
    public void ejectAfterTurnCrank() throws Exception {
        insertQuarter();
        turnCrank();
        insertQuarter();
        turnCrank();
        ejectQuarter();
        int newcount = fsm.getCount();

        assertTrue(newcount < 5 && newcount >= 1);

    }

    @Test
    public void insertTwoQuarters() throws Exception {
        insertQuarter();
        insertQuarter();
        turnCrank();

        insertQuarter();
        turnCrank();

        insertQuarter();
        turnCrank();

        ejectQuarter();
        int newcount = fsm.getCount();

        assertTrue(newcount < 5 && newcount >= 0);

    }

}

