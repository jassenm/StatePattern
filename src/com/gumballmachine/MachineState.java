package com.gumballmachine;

import java.util.Random;

/**
 * Created by jassenmoran on 9/17/15.
 */
public enum MachineState implements State {
    SOLD_OUT {
        public void insertQuarter(TurnstileFSM fsm) {
            System.out.println("You can't inserted a quarter, the machine is sold out");
        }
        public void ejectQuarter(TurnstileFSM fsm) {
            System.out.println("You can't eject, you haven't inserted a quarter yet");
        }
        public void turnCrank(TurnstileFSM fsm) {
            System.out.println("You turned, but there are no gumballs");
        }

        public void dispense(TurnstileFSM fsm) {
            System.out.println("No gumball dispensed");
        }
    },
    NO_QUARTER {
        public void insertQuarter(TurnstileFSM fsm) {
            fsm.setState(HAS_QUARTER);
        }
        public void ejectQuarter(TurnstileFSM fsm) {
            System.out.println("You haven't inserted a quarter");
        }
        public void turnCrank(TurnstileFSM fsm) {
            System.out.println("You turned, but there's no quarter");
        }

        public void dispense(TurnstileFSM fsm) {
            System.out.println("You need to pay first!!");
        }
    },
    HAS_QUARTER {
        Random randomWinner = new Random(System.currentTimeMillis());

        public void insertQuarter(TurnstileFSM fsm) {
            System.out.println("You can't insert another quarter");
        }
        public void ejectQuarter(TurnstileFSM fsm) {
            System.out.println("Quarter returned");
            fsm.setState(NO_QUARTER);
        }
        public void turnCrank(TurnstileFSM fsm) {
            System.out.println("You turned...");
            int winner = randomWinner.nextInt(10);
            if ((winner == 0) && (fsm.getCount() > 1)) {
                fsm.setState(WINNER);
            }
            else {
                fsm.setState(SOLD);
            }
        }
        public void dispense(TurnstileFSM fsm) {
            System.out.println("No gumball dispensed");
        }
    },
    SOLD {
        public void insertQuarter(TurnstileFSM fsm) {
            System.out.println("Please wait, we're already giving you a gumball");
        }
        public void ejectQuarter(TurnstileFSM fsm) {
            System.out.println("Sorry, you already turned the crank");
        }
        public void turnCrank(TurnstileFSM fsm) {
            System.out.println("Turning twice doesn't get you another gumball!");
        }
        public void dispense(TurnstileFSM fsm) {
            fsm.releaseBall();
            if (fsm.getCount() > 0) {
                fsm.setState(NO_QUARTER);
            }
            else {
                System.out.println("Oops, out of gumballs!");
                fsm.setState(SOLD_OUT);
            }
        }
    },
    WINNER {
        public void insertQuarter(TurnstileFSM fsm) {
            System.out.println("Please wait, we're already giving you a gumball");
        }
        public void ejectQuarter(TurnstileFSM fsm) {
            System.out.println("Sorry, you already turned the crank");
        }
        public void turnCrank(TurnstileFSM fsm) {
            System.out.println("Turning twice doesn't get you another gumball!");
        }
        public void dispense(TurnstileFSM fsm) {
            System.out.println("YOU'RE A WINNER! You get two gumballs for your quarter");
            fsm.releaseBall();
            if (fsm.getCount() == 0) {
                fsm.setState(SOLD_OUT);
            }
            else {
                fsm.releaseBall();
                if (fsm.getCount() > 0)
                {
                    fsm.setState(NO_QUARTER);
                }
                else {
                    System.out.println("Oops, out of gumballs!");
                    fsm.setState(SOLD_OUT);
                }
            }
        }
    }
}