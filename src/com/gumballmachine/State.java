package com.gumballmachine;

/**
 * Created by jassenmoran on 9/17/15.
 */
public interface State {
    void insertQuarter(TurnstileFSM fsm);
    void ejectQuarter(TurnstileFSM fsm);
    void turnCrank(TurnstileFSM fsm);
    void dispense(TurnstileFSM fsm);
}
