package com.turnstile;

/**
 * Created by jassenmoran on 9/17/15.
 */
public interface TurnstileState {
    public void pass(TurnstileFSM fsm);
    public void coin(TurnstileFSM fsm);
}
