package com.turnstile;

/**
 * Created by jassenmoran on 9/17/15.
 */
public enum OneCoinTurnstileState implements TurnstileState {
    LOCKED {
        public void pass(TurnstileFSM fsm) {fsm.alarm(); }

        public void coin(TurnstileFSM fsm) {
            fsm.setState(UNLOCKED);
            fsm.unlock();
        }
    },
    UNLOCKED {
        public void pass(TurnstileFSM fsm) {
            fsm.setState(LOCKED);
            fsm.lock();
            System.out.println("running");
        }
        public void coin(TurnstileFSM fsm) { fsm.thankyou(); }
    }
}

