package com.turnstile;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

/**
 * Created by jassenmoran on 9/17/15.
 */

public class NoQuarterStateTest extends TurnstileFSM{
    TurnstileFSM fsm;
    String actions = "";

    @Before
    public void setup() {
        fsm = this;
        fsm.setState(OneCoinTurnstileState.LOCKED);
        actions = "";
    }

    private void assertActions(String expected) {
        assertEquals(expected, actions);
    }

    @Test
    public void normalOperation() throws Exception {
        coin();
        assertActions("U");
        pass();
        assertActions("UL");
    }

    @Test
    public void doublePayment() throws Exception {
        coin();
        coin();
        assertActions("UT");
    }

    @Test
    public void manyCoins() throws Exception {
        for (int i=0; i<5; i++)
            coin();
        assertActions("UTTTT");
    }

    @Test
    public void manyCoinsThenPass() throws Exception {
        for (int i=0; i<5; i++)
            coin();
        pass();
        assertActions("UTTTTL");
    }

    @Test
    public void forcedEntry() throws Exception {
        pass();
        assertActions("A");
    }

    public void unlock() { actions += "U"; }
    public void alarm() { actions += "A"; }
    public void thankyou() { actions += "T"; }
    public void lock() { actions += "L"; }
}

