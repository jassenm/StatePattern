package com.turnstile;

/**
 * Created by jassenmoran on 9/17/15.
 */
public abstract class TurnstileFSM {
    private TurnstileState state;

    public void pass() { state.pass(this); }
    public void coin() { state.coin(this); }
    public void setState(TurnstileState state) { this.state = state;}

    public abstract void alarm();
    public abstract void lock();
    public abstract void unlock();
    public abstract void thankyou();
}

